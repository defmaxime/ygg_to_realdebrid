module.exports = {
  FILE_TYPE: Object.freeze({CONFIG:'config.json', CREDENTIALS:'credentials.json'}),
  read: function(param) {
    if(typeof(param) == 'undefined'){
      console.error("getConfig() : define param with a JSON.FILE_TYPE");
      process.exit();
    }
    if(param != this.FILE_TYPE.CONFIG && param != this.FILE_TYPE.CREDENTIALS ){
      console.error("getConfig() : define param with a JSON.FILE_TYPE");
      process.exit();
    }
    const fs = require('fs');
    JSON_output = JSON.parse(fs.readFileSync(param));
    console.debug("getConfig() : file \""+param+"\" has been successfully read");
    return JSON_output
  }
};
