const RequestsHelper = require('./RequestsHelper');
const qs = require('querystring');

module.exports = {
  postToRD: async function(api_token,magnet) {
    if(typeof api_token === 'undefined' && typeof magnet === 'undefined'){return 'api_token is undefined';}

    let rd_result= await this.postMagnet(api_token,magnet)
    let added_result_id = rd_result.data.id;
    let rd_torrent_add = await this.validateTorrentFiles(api_token,added_result_id);

    let toto=1
    
  },
  postMagnet: async function(api_token,magnet) {
    let options={
      headers : {'Authorization':'Bearer '+api_token},
      url     : 'https://api.real-debrid.com/rest/1.0/torrents/addMagnet',
      method  : 'POST',
      maxRedirects:0,
      data    : qs.stringify({ magnet: magnet })
    };
    let result = await RequestsHelper.HTTPS(options).catch(
      (err) => {
        if(err.response.status !== 201)
        {
          console.log('send to Real debrid failed :\'(');
          return;
        }
      }
    );
    console.debug("postMagnet() : magnet link successfully sent to realdebrid");
    return result;
  },
  validateTorrentFiles: async function (api_token,torrent_id) {
    let requestBody = {files:"all"};
    let options={
      headers : {'Authorization':'Bearer '+api_token},
      url     : 'https://api.real-debrid.com/rest/1.0/torrents/selectFiles/'+torrent_id,
      method  : 'POST',
      maxRedirects:0,
      data    : qs.stringify(requestBody)
    }
    let file_select_result = await RequestsHelper.HTTPS(options).catch(
      (err) => {
        if(err.response.status !== 201)
        {
          console.log('Selecting files for torrent id '+torrent_id+' on Real debrid failed :\'(');
          return;
        }
      }
    );
    console.debug("validateTorrentFiles() : files validated with success");
    return file_select_result;
  }
};
