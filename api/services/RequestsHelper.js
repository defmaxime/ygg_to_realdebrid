const RegexHelpers = require('./RegexHelpers');

module.exports = {
  HTTPS: function (options) {
    const axios = require('axios');
    return new Promise((resolve,reject) => {
      axios(options)
			.then((response) => {
			  resolve(response);
			})
			.catch((result) => {
        reject(result);
			});
    });
  },
  get_flaresolverr_url: function (input) {
    return input["protocol"]+'://' + input["IP"] + ':' + input["port"] + input["path"]
  }
};
