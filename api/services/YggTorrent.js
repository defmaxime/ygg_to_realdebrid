const { FILE_TYPE } = require("./JSONhelper");
const JSONhelper = require("./JSONhelper");
const RegexHelpers = require("./RegexHelpers");
const RequestsHelper = require("./RequestsHelper");

module.exports = {
  getHashInfo: async function(link) {
    console.log("request received from web client");
    let full_result = await YggTorrent.getYggPage(link);
    let hashInfo = RegexHelpers.getInfoHash(full_result.data.solution.response);
    console.debug("getHashInfo() : hash info retrieved from yggtorrent : "+hashInfo);
    return hashInfo
  },
  getYggPage: async function(link) {
    let flaresolverr_details = JSONhelper.read(FILE_TYPE.CONFIG);
    let options = {
      method: 'post',
      url: RequestsHelper.get_flaresolverr_url(flaresolverr_details),
      data: '{\
        "cmd": "request.get"\,\
        "session": "'+flaresolverr_details["session_id"]+'"\,\
        "url":"'+link+'"\,\
        "maxTimeout": 60000\
      }',
      headers: {"Content-Type": "application/json"}
    }
    let full_result = await RequestsHelper.HTTPS(options);
    if("error" == full_result.data.status){
      process.exit();
    }
    console.debug("getYggPage() : request page from yggtorrent has been returned");
    return full_result
  }
};
