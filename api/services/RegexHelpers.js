module.exports = {
  getInfoHash: function(content) {
    if( typeof content == 'undefined'){return "content is undefined";}
    return content.split('<td>Info Hash</td>\n')[1].split('<td>')[1].split('</td>')[0];
  }
};
