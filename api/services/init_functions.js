const fs = require('fs');
const JSONhelper = require("./JSONhelper");
const {FILE_TYPE} = require('./JSONhelper');

module.exports = {
  update_flaresolverr_config: function(input, key) {
    let flaresolverr_config = JSONhelper.read(FILE_TYPE.CONFIG)
    flaresolverr_config[key] = input
    value = JSON.stringify(flaresolverr_config)
    fs.writeFileSync(FILE_TYPE.CONFIG, value)
    console.log('Flaresolverr field \"'+key+'\" has been updated to \"'+flaresolverr_config[key]+'\"');
  },
  check_credentials_env_variables: function () {
    if ((typeof (process.env.yggtorrent_passkey) !== 'undefined') && (typeof (process.env.real_debrid_api_token) !== 'undefined')) {
      // If there are env variables defined then :
      let config_details = { yggtorrent_passkey: process.env.yggtorrent_passkey, real_debrid_api_token: process.env.real_debrid_api_token };
      fs.writeFileSync(FILE_TYPE.CREDENTIALS, JSON.stringify(config_details))
      console.log('Crendentials from env variables stored');
      return true;
    } else {
      console.log('No crendentials from env variables found');
      return false;
    }
  },
  check_credentials_file: function () {
    // If there are no env variables defined then :
    // Check if "credentials.json" file exists before running the server (file containing credentials)
    fs.stat(FILE_TYPE.CREDENTIALS, (err) => {
      if (err !== null) {
        if (err.code === 'ENOENT') {
          // If there's no credentials file
          console.error('\n\n----------------\n\n"'+FILE_TYPE.CREDENTIALS+'" file does not exist !\n\nYou need to copy/paste "credentials_template.json" in a "'+FILE_TYPE.CREDENTIALS+'" file and fill it up with your credentials or set environment variables "real_debrid_api_token" and "yggtorrent_passkey" with your credentials');
          process.exit(1);
        }
      }
    });
    console.log('file "'+FILE_TYPE.CREDENTIALS+'" detected');
  },
  check_config_env_variables: function () {
    if (typeof (process.env.flaresolverr_protocol) !== 'undefined'){
      this.update_flaresolverr_config(process.env.flaresolverr_protocol,"protocol")
    }
    if (typeof (process.env.flaresolverr_IP) !== 'undefined'){
      this.update_flaresolverr_config(process.env.flaresolverr_IP,"IP")
    }
    if (typeof (process.env.flaresolverr_port) !== 'undefined'){
      this.update_flaresolverr_config(process.env.flaresolverr_port,"port")
    }
    if (typeof (process.env.flaresolverr_path) !== 'undefined'){
      this.update_flaresolverr_config(process.env.flaresolverr_path,"path")
    }
  },
  check_config_file: function () {
    // Check if "config.json" file exists before running the server (file containing flaresolverr config)
    fs.stat(FILE_TYPE.CONFIG, (err) => {
      if (err !== null) {
        if (err.code === 'ENOENT') {
          // If there's no config file
          console.error('\n\n----------------\n\n"'+FILE_TYPE.CONFIG+'" file does not exist !\n\nYou need to copy/paste "config_template.json" in a "'+FILE_TYPE.CONFIG+'" file and fill it up with your flaresolverr config');
          process.exit(1);
        }
      }
    });
    console.log('file "'+FILE_TYPE.CONFIG+'" detected');
    console.log(JSONhelper.read(FILE_TYPE.CONFIG));
  },
  init_flaresolverr: async function () {
    console.log('Running flaresolverr init');

    const RequestsHelper = require('./RequestsHelper');
    const JSONhelper = require("./JSONhelper");
    const { FILE_TYPE } = require("./JSONhelper");

    let flaresolverr_details = JSONhelper.read(FILE_TYPE.CONFIG);
    let options = {
      method: 'post',
      url: RequestsHelper.get_flaresolverr_url(flaresolverr_details),
      data: '{"cmd": "request.get","url":"http://www.google.fr"}',
      headers: {"Content-Type": "application/json"},
      timeout: 10000,
    }
    console.log("contacting flaresolverr server at "+RequestsHelper.get_flaresolverr_url(flaresolverr_details));
    let success=false;
    for (let index = 0; index < 10 && success==false; index++) {
      try {
        (await RequestsHelper.HTTPS(options)).data.sessions;
        success = true;
      } catch (error) {
        console.log("failed to get test request, retrying (test "+index+" )");
      }
    }

    if(success==false){
      console.log("   ---   fatally failed to get test request, aborting program");
      process.exit();
    }
  }
};
