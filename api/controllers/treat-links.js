const JSONhelper = require("../services/JSONhelper");
const RealDebrid = require("../services/RealDebrid");
const YggTorrent = require("../services/YggTorrent");

module.exports = {


  friendlyName: 'Treat ygg links',
  exits: {
    success: {
      statusCode: 200,
      description: 'The link has been successfully treated.'
    }

  },
  fn: async function (req,res) {
    let credentials = JSONhelper.read(JSONhelper.FILE_TYPE.CREDENTIALS);
    let link = this.req.allParams()["link"];
    let hashInfo = await YggTorrent.getHashInfo(link);
    let magnetlink="magnet:?xt=urn:btih:"+hashInfo+"&tr=http%3a%2f%2fygg.peer2peer.cc%3a8080%2f"+credentials["yggtorrent_passkey"]+"%2fannounce";
    await RealDebrid.postToRD(credentials['real_debrid_api_token'],magnetlink);

  }
};
